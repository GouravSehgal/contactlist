import React, { Component } from "react";
import './home.css';
import { Button } from "reactstrap";
import { contactsData } from "../../utils/dev-data";
import ContactList from "../../components/ContactList";
import ConfirmationModal from "../../components/Modals/ConfirmationModal";
import ContactDetail from "../../components/ContactDetail";
import ContactForm from "../../components/ContactForm";

class Home extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      searchParam: "", contactList: contactsData, activeContact: null,
      formData: {
        type: "Add",
        data: null,
      }, onToggleContactForm: false, onToggleConfirmation: false
    }    
  }

  componentDidMount() {
    this.setState({
      activeContact: this.findFirstActiveContact(),
    });
  }

  findFirstActiveContact = () => {
    const { contactList } = this.state;
    const item = contactList.sort(function compare(a, b) {
      if (a.firstName < b.firstName) return -1;
      if (a.firstName > b.firstName)  return 1;
      return 0;
    }).find( (element) => {
      return element.isActive === true;
    });
    return item ?? null;
  };
  
  handleSearchChange = (e) => {
    this.setState({  searchParam: e.target.value });
  }

  onClickContact = (id) => {
    const { contactList } = this.state;
    const item = contactList.find((element)=> {
      return element.id === id;
    });
    if (item) this.setState({activeContact: item});
  }

  onClickAddContactButton = () => {
    const { onToggleContactForm } = this.state;
    this.setState({
      formData: {
        type: "Add",
        data: null,
      },onToggleContactForm: !onToggleContactForm,
    });
  };
  
  toggleContactModal = () => {
    const { onToggleContactForm } = this.state;
    this.setState({
      onToggleContactForm: !onToggleContactForm,
    });
  };

  deleteContact = () => {
    this.toggleConfirmationModal();
  };

  onDeleteContactConfirm = (id) => {
    const { contactList } = this.state;
    const newList = contactList.map((el) => {
      if (el.id === id)  el.isActive = false;
      return el;
    });
    this.setState({contactList: newList,}, () => {
        this.toggleConfirmationModal();
        this.setState({
          activeContact: this.findFirstActiveContact(),
        });
      }
    );
  };
  
  toggleConfirmationModal = () => {
    const { onToggleConfirmation } = this.state;
    this.setState({
      onToggleConfirmation: !onToggleConfirmation,
    });
  };

  onEditContact = () => {
    const { activeContact, onToggleContactForm } = this.state;
    this.setState({
      formData: {
        type: "Edit",
        data: activeContact,
      },onToggleContactForm: !onToggleContactForm,
    });
  };
  
  addToContactList = (postData) => {
    const { contactList } = this.state;
    if (postData.errors) delete postData.errors;
    const ids = contactList.map((element) => element.id);
    const maxId = Math.max(...ids);
    postData.id = maxId + 1;
    postData.isActive = true;

    this.setState({contactList: [...contactList, postData],},() => {
        this.setState({activeContact: this.findFirstActiveContact(),});
      }
    );
  };

  editContact = (postData) => {
    const { contactList } = this.state;
    if (postData.errors) delete postData.errors;
    const newList = contactList.map((el) => {
      if (el.id === postData.id)return postData;
      else return el;
    });
    this.setState({ contactList: newList, activeContact: postData });
  };

  render() {
    return (
      <div className="main-section">
        {/* Main Body BOC*/}
        <div className="main-body">

          {/* Left Section BOC*/}
          <div className="list">
            
            {/* Search Field BOC*/}
            <div className="form-group has-search">
              <span className="fa fa-search form-control-feedback"></span>
              <input type="text" className="form-control" placeholder="Search User" value={this.state.searchParam} onChange={this.handleSearchChange}
              />
            </div>
            {/* Search Field EOC*/}

            {/* Contact List BOC*/}
            <ContactList
              contacts={this.state.contactList}
              searchParam={this.state.searchParam}
              onClickContact={this.onClickContact}
              activeContact={this.state.activeContact}
            />
            {/* Contact List EOC*/}

            {/* Add Button Section BOC*/}
            <div className="add-btn">
              <Button color="danger" className="btn-block mt-3" onClick={this.onClickAddContactButton}
              >ADD</Button>
            </div>
            {/* Add Button Section EOC*/}
          </div>
          {/* Left Section EOC*/}

          {/* Right Section Contact Detail BOC*/}
          <ContactDetail
            activeContact={this.state.activeContact}
            deleteContact={this.deleteContact}
            editContact={this.onEditContact}
          />
          {/* Right Section EOC*/}
        </div>
        {/* Main Body BOC*/}

        {/* Add/Edit Contact Form BOC*/}
        <ContactForm
          formData={this.state.formData}
          addToContactList={this.addToContactList}
          editContact={this.editContact}
          isOpen={this.state.onToggleContactForm}
          toggleContactModal={this.toggleContactModal}
          allContacts={this.state.contactList}
        />
        {/* Add/Edit Contact Form EOC*/ }       
        
        {/* Delete Contact Modal BOC*/}        
        <ConfirmationModal
          isOpen={this.state.onToggleConfirmation}
          toggleModal={this.toggleConfirmationModal}
          onConfirm={() => this.onDeleteContactConfirm(this.state.activeContact.id)}
          text="Are you sure!"
        />
        {/* Delete Contact Modal EOC*/} 
      </div>
    );
  }
}

export default Home;
