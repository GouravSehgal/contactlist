import React, { memo } from "react";
import "./contactList-style.css";
import PropTypes from "prop-types";
import { Button } from "reactstrap";

const ContactList = (props) => {
  const { contacts, searchParam, onClickContact, activeContact } = props;
  const filteredObj = {};

  const filteredData = (prevData) => {
    if (prevData && prevData.length > 0) {
      let sortedData = prevData.filter((el) => el.isActive === true).sort(function compare(a, b) {
          if (a.firstName < b.firstName)  return -1;          
          if (a.firstName > b.firstName) return 1;
          return 0;
        });

      if (searchParam) {
        sortedData = sortedData.filter((el) =>
          `${el.firstName} ${el.middleName} ${el.lastName}`.toLowerCase().includes(searchParam.toLowerCase())
        );
      }

      sortedData.forEach((element) => {
        const firstChar = element.firstName.toLowerCase().charAt(0);
        if (filteredObj[firstChar]) filteredObj[firstChar] = [...filteredObj[firstChar], element.id];
        else filteredObj[firstChar] = [element.id];        
      });

      const mappedData = Object.keys(filteredObj).map((element) => {
        return {
          category: element.toUpperCase(),
          data: sortedData.filter((it) =>
            filteredObj[element.toLowerCase()].includes(it.id)
          ),
        };
      });
      return mappedData;
    }
  };

  return (
    <div className="list-menu">
      {
        filteredData(contacts).map((el, index) => {
          return(
            <div className="text-a" key={`out-${index}`}>
              <h3>{el.category}</h3>
              <ul>
                {
                  el.data.map((item) => (
                  <li key={`in-${item.id}`}>
                    <Button color={
                        activeContact?.id === item.id ? "primary btn-block mb-2": "link"
                      } onClick={() => {onClickContact(item.id);}}>
                      {item.firstName + " "}
                      {item?.middleName ? `${item?.middleName.charAt(0)}.` : ""}
                      {" " + item.lastName}
                    </Button>
                  </li>
                ))}
              </ul>
            </div>
          )
        })
      }
    </div>
  );
};

ContactList.propTypes = {
  contacts: PropTypes.array,
  searchParam: PropTypes.string,
  onClickContact: PropTypes.func,
  activeContact: PropTypes.object,
};

export default memo(ContactList);
