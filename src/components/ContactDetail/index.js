import { Button } from "reactstrap";
import "./contactDetail-style.css";
import PropTypes from "prop-types";
import dummyImage from "../../assets/images/dummyimage.jpg";

const ContactDetail = (props) => {
  const { activeContact, deleteContact, editContact } = props;

  return (
  
    <div className="right-sec">
      {
        activeContact 
        ? 
        (
          <>
            <div className="user-details">
              <div className="display-img">
                <img src={dummyImage} className="pp-photo" alt=""></img>
              </div>
              <div className="user-name">
                <h3>
                  {activeContact.firstName + " " + activeContact?.middleName + " " + activeContact.lastName}
                </h3>
                <div className="mail-cont">
                  <div className="d-flex mt-5">
                    <div className="mr-4">
                      <b>Email: </b>{" " + activeContact.email}
                    </div>
                    
                  </div>
                  <div className="d-flex mt-3">
                    <div className="mr-4">
                      <b>Group Name: </b>{" " + activeContact.group}
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
            <div className="btn-footer pl-3">
              <div className="btn-left d-flex mx-n2">
                <Button className="mx-2" color="primary"  onClick={editContact}
                > Edit</Button>
              </div>
              <div className="btn-left mt-2 ml-auto">
                <Button color= 'danger' className="mr-3" block onClick={deleteContact}>Delete</Button>
              </div>
            </div>
          </>
        ) 
        : 
        (
          "No Data Found."
        )
      }
    </div>
  );
};

ContactDetail.propTypes = {
  activeContact: PropTypes.object,
  deleteContact: PropTypes.func,
  editContact: PropTypes.func,
};

export default ContactDetail;
