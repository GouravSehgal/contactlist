import validator from "validator";

export const ValidateData = (min = 1, max = 20, val = "", req = true) => {
  let error = "";
  if (!val && req) {
    error = "Can not be empty";
  } else if (val) {
    if (val.length < min) error = `Min. length is ${min}`;
    else if (val.length > max) error = `Max. length is ${max}`;
    else if (!validator.isAlpha(val)) error = "Only alphabets accepted";    
  }
  return error;
};

export const ValidateEmail = (val = "", allContacts, id, req = true) => {
  let error = "";
  if (!val && req) error = "Can not be empty";
   else if (val) {
    if (!validator.isEmail(val)) error = "Invalid Email";
    if (allContacts && allContacts.length > 0) {
        const item = allContacts.find(function (el) {
        return el.isActive && el.email === val.toLowerCase();
      });
      if (item && item.id !== id) error = "Email Id already exist";
    }
  }
  return error;
};
